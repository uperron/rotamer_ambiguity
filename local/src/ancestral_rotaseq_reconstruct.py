#!/usr/bin/env python

from sys import stdin, stderr
import argparse
import numpy as np
from ruamel.yaml import YAML
yaml = YAML()

from collections import namedtuple, OrderedDict
import numpy as np
import scipy.linalg
import pandas as pd
from dendropy import Tree, TaxonNamespace, Taxon
import random
import string
import warnings
import itertools
import json

# a custom NODE obj
NODE = namedtuple('NODE', ['Cx_dict', 'Lx_dict'])
#Lx(i)is the likelihood of the best reconstruction of this subtree 
#on the condition that the father
#of node x (the current node) is assigned character state i.
#Cx(i) is the character state assigned to node x (the current node) 
#in this optimal conditional reconstruction.

def main():
        usage = "Reconstruct internal node rotasequences using the Joint Reconstruction algorithm from Pupko et al. 2000 \
                http://www.ncbi.nlm.nih.gov/pubmed/10833195. Handles mixed data in input (AA sequences or rotasequences)"

        parser = argparse.ArgumentParser(description=usage)
        parser.add_argument('treefile', nargs=None, type=str, help=".newick phylogeny to be used for reconstruction")
        parser.add_argument('alignfile',  nargs=None, type=str, help=".PHYLIP alignment (rotasequences, AA sequences or both) to be used for reconstruction")
        parser.add_argument('IRMfile',  nargs=None, type=str, help="scaled Q_matrix (1 AA substitution per unit of time)")
        parser.add_argument('freqfile',  nargs=None, type=str, help="state equilibrium frequencies)")
        parser.add_argument('dictfile',  nargs=None, type=str, help="dictionary file)")
        parser.add_argument('-e', '--exhaustive', action="store_true", default=False, help='Iter over all states not only those observed at site')
        parser.add_argument('-A', '--AA_model', action="store_true", default=False, help='use AA states as definite states in aq 20-state model')
        parser.add_argument('-c', '--constrained', default=False, help='Constrain rotamer states reconstruction using internal AA sequences.')
        parser.add_argument('-i', '--ignore_leaf', type=str, default=False, help='Ignore leaf sequence (i.e. treat leaf as internal node)')
        parser.add_argument('-I', '--Ignore_and_trim_sibling', type=str, default=False, help='Ignore leaf sequence and do not use Lx from its sibling (i.e. treat leaf as root)')
        parser.add_argument('-p', '--print_pseudo_L',  type=str,  default=False, help='Print to file the likelihood of the best reconstruction of the subtree below NODE coonditional on NODE being assigned a given state S')
        args = parser.parse_args()

        with open(args.dictfile, "r") as infile:
                data = yaml.load(infile)
        ambiguity_dict = dict(data['AMBIGUITY_DICT'])
        ambiguous_states = list(data['AA_STATES'])
        definite_states = list(data['ROTA_STATES'])
        
        if args.AA_model:
                definite_states = list(data['AA_STATES'])
                ambiguous_states = []

        if args.Ignore_and_trim_sibling:
                args.ignore_leaf = args.Ignore_and_trim_sibling
        
        tns = TaxonNamespace(is_case_sensitive=True)

        # dedropy creates a fake root if tree is unrooted
        tree = Tree.get(path=args.treefile, schema='newick', taxon_namespace=tns, case_sensitive_taxon_labels=True, suppress_internal_node_taxa=False)
        
        # assign taxon instances (and corresponding randomly-generated labels) 
        # to internal nodes if not present
        taxonless_intnodes = [intnode for intnode in tree.postorder_internal_node_iter() if intnode.taxon is None and intnode._parent_node is not None]
        if len(taxonless_intnodes) >0:
                warnings.warn('Internal node taxon labels not found in %s, generating random labels...' % (args.treefile))              
                symbols = string.ascii_letters
                intnode_names = [''.join([random.choice(symbols) for i in range(5)]) for j in range(len(taxonless_intnodes))]
                for intnode, intnode_name in zip(taxonless_intnodes, intnode_names):
                        intnode.taxon = Taxon(label=intnode_name)
                        warnings.warn('Assigned label %s to node %s' % (intnode.taxon.label, str(intnode)))
                root = [intnode for intnode in tree.postorder_internal_node_iter() if intnode._parent_node is None][0]
                root.taxon = Taxon(label='ROOT')
                warnings.warn('Assigned label %s to node %s' % (root.taxon.label, str(root)))
        internal_taxons = ['ROOT'] + [intnode.taxon.label for intnode in tree.postorder_internal_node_iter() if intnode._parent_node is not None]       

        # read alignment
        orig_align_df = pd.read_csv(args.alignfile,  sep="\t", header=None, index_col=0, names=["sequence"], skiprows=1)
        taxa_num, align_len = [len(orig_align_df), orig_align_df["sequence"].str.split(' ').str.len().values[0]]
        # split sequences into alignment columns, one per site
        split_align_df = orig_align_df['sequence'].str.split(' ', expand=True).rename(columns = lambda x: str(x+1))

        if args.constrained:
                # read the internal AA sequence alignment
                constraint_align_df = pd.read_csv(args.constrained,  sep="\t", header=None, index_col=0, names=["sequence"], skiprows=1)
                constraint_split_align_df = constraint_align_df['sequence'].str.split(' ', expand=True).rename(columns = lambda x: str(x+1))
        
        if args.ignore_leaf:
                reconstructed_split_align_df = pd.DataFrame(0, index=internal_taxons + [args.ignore_leaf], columns=[])
        else:
                # output alignment of reconstructed sequences from internal nodes
                reconstructed_split_align_df = pd.DataFrame(0, index=internal_taxons, columns=[]) 
        
        if args.print_pseudo_L:
                pseudo_L_arr = []
                                
        # read IRM, needs rows that sums to 0, also needs to be scaled 
        # so that it has on avg 1 AA state change per unit of time at equilibrium

        # overwrite state labels
        IRM_df = pd.read_csv(args.IRMfile, sep="\t", header=None, skiprows=1, names=definite_states, index_col=0)
        IRM_df.index = definite_states
        freqs_df = pd.read_csv(args.freqfile, sep="\t", header=None, skiprows=1, names=definite_states, index_col=None)
        nstates = len(definite_states)

        # from scaled IRM to Q with lines that sum to 0
        #exponentiate Q for every brlen in tree
        # head_node is the child node for this edge
        Pt_dict = {}
        IRM_arr = IRM_df.values
        for i in range(0,nstates):
                IRM_arr[i,i] = (np.sum(IRM_arr[i,:]) - IRM_arr[i,i])*-1 
        for edge in tree.postorder_edge_iter():
                if edge.length != None:
                        Pt_df = pd.DataFrame(scipy.linalg.expm(edge.length* IRM_arr), columns=definite_states, index=definite_states)
                        Pt_dict[edge.head_node.taxon.label] = Pt_df
                        

        for site in [str(x) for x in range(1, align_len+ 1)]:
                # remove gap character
                if args.exhaustive:
                        obs_states = definite_states
                #elif args.constrained:
                #       # all rotamer states associated to observed AA states allows to consider rotamer states not observed at leaves for internal nodes
                #       list2d = [ambiguity_dict[x] for x in constraint_split_align_df[site].unique() if x != '-']
                #       obs_states = list(itertools.chain(*list2d))
                else:
                        obs_states = []
                        # handle mixed data
                        for state in [s for s in split_align_df[site].unique() if s != '-']:
                                if state in definite_states:
                                        obs_states.append(state)
                                else:
                                        obs_states.extend(ambiguity_dict[state])
                                        
                NODE_dict = {}

                # step 1 (leaves)
                for node in tree.leaf_node_iter():
                        Cx_dict = {}
                        Lx_dict = {}
                        # current state is always the observed state
                        current_state = split_align_df[site][node.taxon.label]
                        # get the corresponding probability matrix
                        Pt_df = Pt_dict[node.taxon.label]
                        for parent_state in obs_states:
                                if args.ignore_leaf == node.taxon.label:
                                        # treat leaf as internal node, allows to evaluate ancestral seq predictions on empirical alignments
                                        # where intnodes haven't been resurrected.
                                        # Done by ingnoring observed sequence, maximizing likelihood (Pt really as no childnodes) 
                                        # and assigning most likely current state for each parent state
                                        max_dict = {}
                                        for possible_state in obs_states:
                                                # a leaf has no children nodes so Pt is the only factor 
                                                max_dict[possible_state] = Pt_df[possible_state][parent_state]
                                        max_possible_state = max(max_dict, key=max_dict.get)
                                        maxL = max_dict[max_possible_state]
                                        Cx_dict[parent_state] = max_possible_state
                                        Lx_dict[parent_state] = maxL
                                else:
                                        if current_state == '-':
                                                # set L to 1
                                                # this should carry no information upward
                                                Lx_dict[parent_state] = 1
                                        elif current_state in ambiguous_states:
                                                # process ambiguous states§
                                                # same as args.ignore_leaf BUT Pt is maximized over a the set of ambiguous states
                                                # that map to current_state NOT over all obs_states
                                                max_dict = {}
                                                for possible_state in ambiguity_dict[current_state]:
                                                        # a leaf has no children nodes so Pt is the only factor
                                                        max_dict[possible_state] = Pt_df[possible_state][parent_state]
                                                max_possible_state = max(max_dict, key=max_dict.get)
                                                maxL = max_dict[max_possible_state]
                                                Cx_dict[parent_state] = max_possible_state
                                                Lx_dict[parent_state] = maxL
                                        else:
                                                # TODO is this order arbitrary? (from is row, to is col)
                                                Lx_dict[parent_state] = Pt_df[current_state][parent_state]
                                        Cx_dict[parent_state] = current_state
                        NODE_dict[node.taxon.label] = NODE(Cx_dict, Lx_dict)
                #each internal node visited after its children (postorder traversal)
                for node in tree.postorder_internal_node_iter():
                        if node._parent_node is None:
                                root = node
                                # visit root, assign state (step 3)
                                # root might have 2 or 3 children
                                childNODEs = [NODE_dict[childnode.taxon.label] for childnode in root.child_node_iter()]
                                max_dict = {}
                                #if args.constrained:
                                #       constrained_states = ambiguity_dict[constraint_split_align_df[site]['ROOT']]
                                #       for current_state in constrained_states:
                                #               max_dict[current_state] = freqs_df[current_state].values[0] * np.prod(np.array([childNODE.Lx_dict[current_state] for childNODE in childNODEs]))
                                #else:
                                # "collapsing AA reconstruction" approach: obs_states are collapsed root states
                                for current_state in obs_states:
                                        max_dict[current_state] = freqs_df[current_state].values[0] * np.prod(np.array([childNODE.Lx_dict[current_state] for childNODE in childNODEs]))
                                #add a column to the reconstructed alignment    
                                reconstructed_split_align_df[site] = ['NA']*len(reconstructed_split_align_df)
                                reconstructed_split_align_df[site]['ROOT'] = max(max_dict, key=max_dict.get)
                                if args.print_pseudo_L:
                                        # add  row to the pseudo_L file
                                        sorted_d = OrderedDict(sorted(max_dict.items(), key=lambda t:  t[1], reverse=True))
                                        # this preserves the order and can be parsed from string
                                        sorted_d_str = json.dumps(sorted_d) 
                                        rec_state = max(max_dict, key=max_dict.get)
                                        rec_state_L = sorted_d[rec_state]
                                        pseudo_L_arr.append(['ROOT', site, rec_state, rec_state_L, sorted_d_str])       
                        else:
                                # visit internal node (not root), perform step 2
                                Cx_dict = {}
                                Lx_dict = {}
                                Pt_df = Pt_dict[node.taxon.label]
                                # get the child NODEs
                                childNODE_1, childNODE_2 = [NODE_dict[childnode.taxon.label] for childnode in node.child_node_iter()]
                                
                                for parent_state in obs_states:
                                        # max L | parent state
                                        max_dict = {}
                                        #if args.constrained:
                                                # reference reconstructed AA state for internal node 
                                                # used to constrainrotamer state reconstruction
                                                # to a subset of rotamer states to the reference AA state
                                                #constrained_states = ambiguity_dict[constraint_split_align_df[site][node.taxon.label]]
                                                #for current_state in constrained_states:
                                                #       max_dict[current_state] = Pt_df[current_state][parent_state] * childNODE_1.Lx_dict[current_state] * childNODE_2.Lx_dict[current_state]
                                        if args.Ignore_and_trim_sibling in [childnode.taxon.label for childnode in node.child_node_iter()]:
                                                childNODE_1 = NODE_dict[args.Ignore_and_trim_sibling]
                                                for current_state in obs_states:
                                                        # don't consider Lx from sibling of ignored node
                                                        max_dict[current_state] = Pt_df[current_state][parent_state] * childNODE_1.Lx_dict[current_state]
                                        else:
                                                for current_state in obs_states:
                                                        max_dict[current_state] = Pt_df[current_state][parent_state] * childNODE_1.Lx_dict[current_state] * childNODE_2.Lx_dict[current_state]
                                        max_current_state = max(max_dict, key=max_dict.get)
                                        maxL = max_dict[max_current_state]
                                        Cx_dict[parent_state] = max_current_state
                                        Lx_dict[parent_state] = maxL
                                NODE_dict[node.taxon.label] = NODE(Cx_dict, Lx_dict)
                        
                # assign states down the tree (step 5) to internal nodes based on state assigned to parent node
                nodes = [node for node in tree.preorder_internal_node_iter() if node is not root]
                if args.ignore_leaf:
                        # also reconstruct ignored leaf
                        nodes = nodes + [node for node in tree.leaf_node_iter() if args.ignore_leaf == node.taxon.label]
                for node in nodes:
                        if node.parent_node is root:
                                parent_assigned_state = reconstructed_split_align_df[site]['ROOT']
                        else:
                                parent_assigned_state = reconstructed_split_align_df[site][node.parent_node.taxon.label]
                        current_assigned_state = NODE_dict[node.taxon.label].Cx_dict[parent_assigned_state]     
                        reconstructed_split_align_df[site][node.taxon.label] = current_assigned_state
                        if args.print_pseudo_L:
                                # create a sorted dictionary {state_for_node : pseudoL}
                                d1 = NODE_dict[node.taxon.label].Cx_dict
                                d2 = NODE_dict[node.taxon.label].Lx_dict
                                unsorted_d = dict([(k, d2[k]) for k in d1.keys()])
                                sorted_d = OrderedDict(sorted(unsorted_d.items(), key=lambda t: t[1], reverse=True))
                                sorted_d_str = json.dumps(sorted_d)
                                rec_state = current_assigned_state
                                rec_state_L = sorted_d[rec_state]
                                pseudo_L_arr.append([node.taxon.label, site, rec_state, rec_state_L, sorted_d_str])     

        if args.print_pseudo_L:
                pseudo_L_df = pd.DataFrame(pseudo_L_arr, columns=["taxon", "site", "rec_state", "rec_state_pseudo_L", "pseudo_L_dict"])
                pseudo_L_df.to_csv(args.print_pseudo_L, header=True, index=False, sep="\t")

        reconstructed_split_align_df['sequences'] = reconstructed_split_align_df.iloc[:,0:align_len].apply(lambda x: ' '.join(x), axis=1)
        reconstructed_split_align_df['taxons'] = reconstructed_split_align_df.index

        print("{}\t{}".format(len(reconstructed_split_align_df), align_len))
        print
        if args.ignore_leaf:    
                # print only the reconstructed leaf sequence
                print("{}\t{}".format(args.ignore_leaf, reconstructed_split_align_df.loc[args.ignore_leaf]['sequences']))
        else:
                print(reconstructed_split_align_df.to_csv(header=False, index=False, sep="\t", columns=['taxons', 'sequences']))
        
if __name__ == '__main__':
        main()

