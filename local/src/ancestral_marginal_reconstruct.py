#!/usr/bin/env python

from sys import stdin, stderr
import argparse
import numpy as np
from ruamel.yaml import YAML
yaml = YAML()

from collections import namedtuple, OrderedDict
from dendropy import Tree, TaxonNamespace, Taxon
import random
import string
import warnings
import itertools
import json



from collections import Counter
import linecache
import os

def eig_expm(A):
	d, Y = np.linalg.eig(A)
	Yinv = np.linalg.pinv(Y)
	D = np.diag(np.exp(d))
	Y = np.asmatrix(Y)
	D = np.asmatrix(D)
	Yinv = np.asmatrix(Yinv)
	B = Y*D*Yinv
	return B


# a custom NODE obj
NODE = namedtuple('NODE', ['Cx_dict', 'Lx_dict'])
#Lx(i)is the likelihood of the best reconstruction of this subtree 
#on the condition that the father
#of node x (the current node) is assigned character state i.
#Cx(i) is the character state assigned to node x (the current node) 
#in this optimal conditional reconstruction.

def main():
	usage = "Reconstruct internal node rotasequences using marginal reconstruction. Handles mixed data in input (AA sequences or rotasequences)"

	parser = argparse.ArgumentParser(description=usage)
	parser.add_argument('treefile', nargs=None, type=str, help=".newick phylogeny to be used for reconstruction")
	parser.add_argument('alignfile',  nargs=None, type=str, help=".PHYLIP alignment (rotasequences, AA sequences or both) to be used for reconstruction")
	parser.add_argument('IRMfile',  nargs=None, type=str, help="scaled Q_matrix (1 AA substitution per unit of time)")
	parser.add_argument('freqfile',  nargs=None, type=str, help="state equilibrium frequencies)")
	parser.add_argument('dictfile',  nargs=None, type=str, help="dictionary file)")
	parser.add_argument('-e', '--exhaustive', action="store_true", default=False, help='Iter over all states not only those observed at site')
	parser.add_argument('-A', '--AA_model', action="store_true", default=False, help='use AA states as definite states in aq 20-state model')
	parser.add_argument('-c', '--constrained', default=False, help='Constrain rotamer states reconstruction using internal AA sequences.')
	parser.add_argument('-i', '--ignore_leaf', type=str, default=False, help='Ignore leaf sequence (i.e. treat leaf as internal node)')
	parser.add_argument('-I', '--Ignore_and_trim_sibling', type=str, default=False, help='Ignore leaf sequence and do not use Lx from its sibling (i.e. treat leaf as root)')
	parser.add_argument('-p', '--print_posterior',  type=str,  default=False, help='Print to file the likelihood of the best reconstruction of the subtree below NODE coonditional on NODE being assigned a given state S')
	args = parser.parse_args()

	with open(args.dictfile, "r") as infile:
		data = yaml.load(infile)
	ambiguity_dict = dict(data['AMBIGUITY_DICT'])
	ambiguous_states = list(data['AA_STATES'])
	definite_states = list(data['ROTA_STATES'])
	
	if args.AA_model:
		definite_states = list(data['AA_STATES'])
		ambiguous_states = []

	if args.Ignore_and_trim_sibling:
		args.ignore_leaf = args.Ignore_and_trim_sibling
	
	tns = TaxonNamespace(is_case_sensitive=True)

	# dedropy creates a fake root if tree is unrooted
	tree = Tree.get(path=args.treefile, schema='newick', taxon_namespace=tns, case_sensitive_taxon_labels=True, suppress_internal_node_taxa=False)

	# assign taxon instances (and corresponding randomly-generated labels) 
	# to internal nodes if not present
	taxonless_intnodes = [intnode for intnode in tree.postorder_internal_node_iter() if intnode.taxon is None and intnode._parent_node is not None]
	if len(taxonless_intnodes) >0:
		warnings.warn('Internal node taxon labels not found in %s, generating random labels...' % (args.treefile))		
		symbols = string.ascii_letters
		intnode_names = [''.join([random.choice(symbols) for i in range(5)]) for j in range(len(taxonless_intnodes))]
		for intnode, intnode_name in zip(taxonless_intnodes, intnode_names):
			intnode.taxon = Taxon(label=intnode_name)
			warnings.warn('Assigned label %s to node %s' % (intnode.taxon.label, str(intnode)))
		root = [intnode for intnode in tree.postorder_internal_node_iter() if intnode._parent_node is None][0]
		root.taxon = Taxon(label='ROOT')
		warnings.warn('Assigned label %s to node %s' % (root.taxon.label, str(root)))
	internal_taxons = ['ROOT'] + [intnode.taxon.label for intnode in tree.postorder_internal_node_iter() if intnode._parent_node is not None]	
	terminal_taxons = [node.taxon.label for node in tree.leaf_node_iter()]

	# read alignment
	with open(args.alignfile, "r") as f:
		split_align_df = {}
		# skip first two rows
		for line in f.readlines()[2:]:
			if line != '\n':
				taxon, seq = line.split('\t')
				split_align_df[taxon] = seq.strip('\n').split(' ')
				
		taxa_num, align_len = [len(internal_taxons), len(line.split("\t")[1].strip('\n').split(' '))]

	if args.constrained:
		# read the internal AA sequence alignment
		constraint_split_align_df = {}
		with open(args.constrained, "r") as f:
			for line in f.readlines()[2:]:
				constraint_split_align_df[line.split("\t")[0]] = line.split("\t")[1].strip('\n').split(' ')
	
	if args.ignore_leaf:
		reconstructed_split_align_df = dict(zip(internal_taxons + [args.ignore_leaf], [[] for t in internal_taxons + [args.ignore_leaf]]))
	else:
		# output alignment of reconstructed sequences from internal nodes
		reconstructed_split_align_df = dict(zip(internal_taxons, [[] for t in internal_taxons]))

	if args.print_posterior:
		with open(args.print_posterior, "w") as f:
			header = ["taxon", "site", "rec_state", "rec_state_posterior", "posterior_dict"]
			print("\t".join(map(str,header)), file=f)

	# overwrite state labels
	nstates = len(definite_states)
	state_index_dict = dict([(k,v) for v,k in enumerate(definite_states)])
	IRM_df = np.zeros((nstates, nstates))
	with open(args.IRMfile, "r") as f:
		for i, line in enumerate(f.readlines()[1:]):
			IRM_df[i] = line.strip("\n").split("\t")[1:]
	with open(args.freqfile, "r") as f:
		freqs_df = [float(s) for s in f.readlines()[1].strip("\n").split("\t")]

	# from scaled IRM to Q with lines that sum to 0
	#exponentiate Q for every brlen in tree
	# head_node is the child node for this edge
	Pt_dict = {}
	IRM_arr = IRM_df
	for i in range(0,nstates):
		IRM_arr[i,i] = (np.sum(IRM_arr[i,:]) - IRM_arr[i,i])*-1	
	for edge in tree.postorder_edge_iter():
		if edge.length != None:
			Pt_df = eig_expm(edge.length* IRM_arr)
			Pt_dict[edge.head_node.taxon.label] = Pt_df
	
	def compute_assignments(reconstructed_split_align_df, ignore_leaf=False, trim_sibling=[]):
		
		if args.exhaustive:
			assigned_states_arr = itertools.combinations_with_replacement(definite_states, len(internal_taxons))

		for site in [x for x in range(1, align_len + 1)]:
			P_dict = {}
			assign_dicts = []
			
			if args.exhaustive:
				obs_states = definite_states
			elif args.constrained:
				# all rotamer states associated to observed AA states allows to consider rotamer states not observed at leaves for internal nodes
				list2d = [ambiguity_dict[x] for x in set([constraint_split_align_df[k][site -1] for k in terminal]) if x != '-']
				obs_states = list(itertools.chain(*list2d))
			else:
				obs_states = []
				# handle mixed data
				for state in [s for s in set([split_align_df[k][site -1] for k in terminal_taxons]) if s != '-']:
					if state in definite_states:
						obs_states.append(state)
					else:
						obs_states.extend(ambiguity_dict[state])
			
			if not args.exhaustive:
				assigned_states_arr = itertools.combinations_with_replacement(obs_states, len(internal_taxons))

			for assigned_states in assigned_states_arr:
				NODE_dict = dict(zip(internal_taxons, assigned_states))
				P_list = []
				# leaves
				for node in tree.leaf_node_iter():
					if node.taxon.label == ignore_leaf:
						# treat as internal node
						current_assigned_state = NODE_dict[node.taxon.label]
						Pt_df = Pt_dict[node.taxon.label]
						if node.parent_node.taxon != None:
							parent_assigned_state = NODE_dict[node.parent_node.taxon.label]
						else:
							parent_assigned_state = NODE_dict['ROOT']
						i, j = [state_index_dict[parent_assigned_state], state_index_dict[current_assigned_state]]
						P_list.append(Pt_df[i], [j])
					elif node.taxon.label in trim_sibling:
						# ignore contribution from this node
						continue
					else:
						current_state = split_align_df[node.taxon.label][site -1]
						if current_state  == '-':
							# skip this taxon for current site
							continue
						elif current_state in ambiguous_states:
							# handle ambiguous states
							possible_states = ambiguity_dict[current_state]		
						# get the corresponding probability matrix
						Pt_df = Pt_dict[node.taxon.label]
					
						# get the parent node's assigned state
						parent = node.parent_node
						if parent.parent_node is None:
							parent_assigned_state = NODE_dict['ROOT']
							if current_state in ambiguous_states:
								# sum of the transition probability * parent_state_freq from each ambiguous state to the ROOOT node state
								s = np.sum([Pt_df[state_index_dict[parent_assigned_state], state_index_dict[state]] * freqs_df[state_index_dict[parent_assigned_state]] for state in possible_states])
								P_list.append(s)	
							else:
								P_list.append(Pt_df[state_index_dict[parent_assigned_state], state_index_dict[current_state]] * freqs_df[state_index_dict[parent_assigned_state]])
						else:
							parent_assigned_state = NODE_dict[parent.taxon.label]
							if current_state in ambiguous_states:
								# sum of the transition probabilityfrom each ambiguous state to the parent state
								s = np.sum([Pt_df[state_index_dict[parent_assigned_state], state_index_dict[state]] for state in possible_states])
								P_list.append(s)
							else:
								parent_assigned_state = NODE_dict[parent.taxon.label]
					
								# get the transition probability from assigned parent state to observed tip state
								i, j = [state_index_dict[parent_assigned_state], state_index_dict[current_state]]
								P_list.append(Pt_df[i,j])
				# internal nodes including ROOT
				for node in tree.postorder_internal_node_iter():
					if node._parent_node is None:
						root = node
						current_assigned_state = NODE_dict['ROOT']
						for childnode in root.child_node_iter():
							# not dry
							if childnode.is_internal():
								Pt_df = Pt_dict[childnode.taxon.label]
								childnode_assigned_state = NODE_dict[childnode.taxon.label]
								P_list.append(Pt_df[state_index_dict[current_assigned_state], state_index_dict[childnode_assigned_state]] * freqs_df[state_index_dict[current_assigned_state]])
	
					
					else:
						current_assigned_state = NODE_dict[node.taxon.label]
						for childnode in node.child_node_iter():
							if childnode.is_internal():
								Pt_df = Pt_dict[childnode.taxon.label]
								childnode_assigned_state = NODE_dict[childnode.taxon.label]
								P_list.append(Pt_df[state_index_dict[current_assigned_state], state_index_dict[childnode_assigned_state]])
				# add the probability of this assignment to NODE_dict
				NODE_dict['P_assignment'] = float(np.prod(P_list))
				assign_dicts.append(NODE_dict)
				
			# performs marginal reconstruction:
			# assign the MOST LIKELY STATE to each node INDEPENDETLY of other nodes' assignments
			for nodename in internal_taxons:
				best_state_dict = {}
				for obs_state in obs_states:
					# sum marginal P for all assignmets that have obs_state at node
					s = sum([NODE_dict['P_assignment'] for NODE_dict in assign_dicts if NODE_dict[nodename] == obs_state])
					best_state_dict[obs_state] = s
				# get the assignment that corresponds to  the highest marginal P for this node (fastest method)
				v=list(best_state_dict.values())
				k=list(best_state_dict.keys())
				reconstructed_split_align_df[nodename].append(k[v.index(max(v))])
				if args.print_posterior:
					sorted_d = OrderedDict(sorted(best_state_dict.items(), key=lambda t: t[1], reverse=True))
					sorted_d_str = json.dumps(sorted_d)
					rec_state =  reconstructed_split_align_df[nodename][site -1]
					with open(args.print_posterior, "a") as f:
						out = [nodename, site, rec_state, best_state_dict[rec_state], sorted_d_str]
						print("\t".join(map(str, out)), file=f)
		out_arr = []
		if args.Ignore_and_trim_sibling:
			t = ignore_leaf
			row = t + "\t" + " ".join(map(str, reconstructed_split_align_df[t]))
			out_arr.append(row)
		else:
			for t in internal_taxons:
				row = t + "\t" + " ".join(map(str, reconstructed_split_align_df[t])) 
				out_arr.append(row)		
		return out_arr


	in_df = reconstructed_split_align_df
	print("{}\t{}".format(len(internal_taxons), align_len))
	print("")
	if args.Ignore_and_trim_sibling:
		# iterate over the leaves in file
		for leafname in  open(args.Ignore_and_trim_sibling, "r").readlines()[0].split(","): 	
			ignore_leaf = leafname
			ignored_node = tree.find_node_with_taxon_label(leafname)

			# find the ignored mode's sibling
			trim_sibling = [node.taxon.label for node in ignored_node.sibling_nodes()]
		
			out_df = compute_assignments(in_df, ignore_leaf=ignore_leaf, trim_sibling=trim_sibling)	
			for row in out_df:
				print(row)
	else:
		out_df = compute_assignments(in_df)
		for row in out_df:
			print(row)
	
if __name__ == '__main__':
	main()

