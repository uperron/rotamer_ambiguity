#!/usr/bin/env python   
import sys
from sys import stdin, stderr, stdout
import argparse

from ete3 import Tree
import numpy as np
import scipy.linalg
import math
import random
import pandas as pd

# this is a snakemake external scripts

def main():
        # can directly generate sequences using 1-character alphabet
        LABELS = snakemake.params.symbols.split(",")
        nstates = len(LABELS)

        df = pd.read_csv(snakemake.input.Q_matrix,  sep="\t", header=None, index_col=None, skiprows=1, usecols=range(1, 56))
        orig_matrix = df.values

        # from IRM to Q with lines that sum to 0
        for i in range(0,nstates):
                orig_matrix[i,i] = (np.sum(orig_matrix[i,:]) - orig_matrix[i,i])*-1
        tree = Tree(snakemake.input.treefile, format=1)

        seq_len = int(snakemake.params.length)
        
        # remove header
        freqs = pd.read_csv(snakemake.input.freqs, sep="\t", skiprows=1, header=None, index_col=None).values[0]
        
        if len(freqs) != len(LABELS):
                exit('Submitted frequencies should match states in LABELS but have length {}, {}'.format(len(freqs), len(LABELS)))
        
        # generate ancestral sequence based on freqs
        orig_seq = np.random.choice(LABELS, seq_len, p=freqs)

        if snakemake.params.internal_seqs:
                outfile = open(snakemake.output.term_outfile, "w")
                int_outfile = open(snakemake.output.int_outfile, "w")
        else:
                outfile = open(snakemake.output.outfile, "w")
        # generate a dictionary of sequences accessed by node names
        seq_dict = {}
        # assign the ancestor sequence to the root node 
        tree.get_tree_root().name = "ROOT"
        seq_dict[tree.get_tree_root().name] = orig_seq

        if snakemake.params.gamma:
                # uniquely assign a rate r to every site drawing from a gamma distribution
                # with shape = alpha, scale( aka theta ) = 1 / beta = 1 / alpha, and mean = 1
                shape = float(snakemake.params.gamma)
                scale = 1 / shape
                gamma_rates = np.random.gamma(shape, scale, seq_len)

        # traverse the tree using a preorder strategy (avoiding the root node):
        # 1) Visit the first descendant node, 2) Traverse the left subtree , 3) Traverse the right subtree
        for node in tree.iter_descendants("preorder"):
                # calculate P(t), evolve the parent sequence accordingly
                parent_seq = seq_dict[node.up.name]
                t = node.dist
                new_seq = np.zeros(seq_len, dtype=object)
                if snakemake.params.gamma == False:
                    Pt_matrix = scipy.linalg.expm(t* orig_matrix)
                for site,current_state in enumerate(parent_seq):
                    if snakemake.params.gamma:
                        # coompute a site-specific Pt according to the rate r 
                        t_r = t * gamma_rates[site]
                        Pt_matrix = scipy.linalg.expm(t_r* orig_matrix)
                    P_state = Pt_matrix[LABELS.index(current_state),:]
                    new_state = np.random.choice(LABELS, 1, p=P_state)[0]
                    new_seq[site] = new_state
                seq_dict[node.name] = new_seq
        
        # iterate over the leaves, print a PHYLIP alignment
        leaves = [leaf.name for leaf in tree.iter_leaves()]
        print("{}\t{}".format(len(leaves), len(orig_seq)), file=outfile)
        print("", file=outfile)
        for leaf in leaves:
                sequence = seq_dict[leaf]
                print("{}\t{}".format(leaf, ' '.join(map(str, sequence))), file=outfile)        


        if snakemake.params.internal_seqs:
                int_nodenames = [node.name for node in tree.traverse(strategy='preorder') if node.is_leaf() == False]
                print("#INT_{}\t{}".format(len(int_nodenames), len(orig_seq)), file=int_outfile)
                print('#INT_', file=int_outfile)
                for nodename in int_nodenames:
                        sequence = seq_dict[nodename]
                        print("#INT_{}\t{}".format(nodename, ' '.join(map(str, sequence))), file=int_outfile)
                                
if __name__ == '__main__':
        main()

