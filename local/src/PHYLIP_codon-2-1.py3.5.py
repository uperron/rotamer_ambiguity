#!/usr/bin/env python
from sys import stdin, stderr
import argparse
import re

from ruamel.yaml import YAML
yaml = YAML()

codon_AA_dict = {'ACC': 'GLN', 'ATG': 'PRO', 'AAG': 'ASN', 'AAA': 'ALA', 'ATC': 'PHE', 'AAC': 'ARG', 'ATA': 'MET', 'AGG': 'LEU', 'CCT': 'NA', 'CTC': 'NA', 'AGC': 'ILE', 'ACA': 'CYS', 'AGA': 'HIS', 'CAT': 'VAL', 'AAT': 'ASP', 'ATT': 'SER', 'CTG': 'NA', 'CTA': 'NA', 'ACT': 'GLY', 'CAC': 'TRP', 'ACG': 'GLU', 'CAA': 'THR', 'AGT': 'LYS', 'CCA': 'NA', 'CCG': 'NA', 'CCC': 'NA', 'TAT': 'NA', 'GGT': 'NA', 'TGT': 'NA', 'CGA': 'NA', 'CAG': 'TYR', 'CGC': 'NA', 'GAT': 'NA', 'CGG': 'NA', 'CTT': 'NA', 'TGC': 'NA', 'GGG': 'NA', 'TAG': 'NA', 'GGA': 'NA', 'TAA': 'NA', 'GGC': 'NA', 'TAC': 'NA', 'GAG': 'NA', 'TCG': 'NA', 'TTA': 'NA', 'TTT': 'NA', 'GAC': 'NA', 'CGT': 'NA', 'GAA': 'NA', 'TCA': 'NA', 'GCA': 'NA', 'GTA': 'NA', 'GCC': 'NA', 'GTC': 'NA', 'GCG': 'NA', 'GTG': 'NA', 'TTC': 'NA', 'GTT': 'NA', 'GCT': 'NA', 'TGA': 'NA', 'TTG': 'NA', 'TCC': 'NA', 'TGG': 'NA', 'TCT': 'NA'}

AA_codon_dict = dict([[v,k] for k,v in codon_AA_dict.items() if v != 'NA'])

codon_rotamer_dict = {'CTT': 'LYS3', 'ATG': 'GLN2', 'ACA': 'ASN1', 'ACG': 'ASN3', 'ATC': 'GLN1', 'AAC': 'ARG1', 'ATA': 'CYS3', 'AGG': 'CYS1', 'CCT': 'ILE1', 'ACT': 'ASP1', 'AGC': 'ASP3', 'AAG': 'ARG2', 'AGA': 'ASP2', 'CAT': 'GLY', 'AAT': 'ARG3', 'ATT': 'GLN3', 'CTG': 'LYS2', 'CTA': 'LEU3', 'CTC': 'LYS1', 'CAC': 'GLU2', 'AAA': 'ALA', 'CAA': 'GLU1', 'AGT': 'CYS2', 'CAG': 'GLU3', 'CCG': 'HIS3', 'CCC': 'HIS2', 'TAT': 'TYR2', 'GGT': 'THR1', 'CGA': 'ILE2', 'CCA': 'HIS1', 'CGC': 'ILE3', 'GAT': 'PHE1', 'CGG': 'LEU1', 'TGC': 'TYR1', 'GGG': 'SER3', 'GGA': 'SER1', 'GGC': 'SER2', 'TAC': 'TRP3', 'GAG': 'MET3', 'TCG': 'VAL2', 'GAC': 'MET2', 'CGT': 'LEU2', 'ACC': 'ASN2', 'TCA': 'TYR3', 'GCA': 'PHE2', 'GTA': 'THR2', 'GCC': 'PHE3', 'GTC': 'THR3', 'GCG': 'PRO1', 'GTG': 'TRP1', 'GTT': 'TRP2', 'GCT': 'PRO2', 'TCC': 'VAL1', 'GAA': 'MET1', 'TCT': 'VAL3'}

rotamer_codon_dict = dict([[v,k] for k,v in codon_rotamer_dict.items()])

one_rota_to_four_dict = {'1': 'VAL2', '0': 'VAL1', '2': 'VAL3', 'A': 'ALA', 'C': 'ARG2', 'B': 'ARG1', 'E': 'ASN1', 'D': 'ARG3', 'G': 'ASN3', 'F': 'ASN2', 'I': 'ASP2', 'H': 'ASP1', 'K': 'CYS1', 'J': 'ASP3', 'M': 'CYS3', 'L': 'CYS2', 'O': 'GLN2', 'N': 'GLN1', 'Q': 'GLU1', 'P': 'GLN3', 'S': 'GLU3', 'R': 'GLU2', 'U': 'HIS1', 'T': 'GLY', 'W': 'HIS3', 'V': 'HIS2', 'Y': 'ILE2', 'X': 'ILE1', 'Z': 'ILE3', 'a': 'LEU1', 'c': 'LEU3', 'b': 'LEU2', 'e': 'LYS2', 'd': 'LYS1', 'g': 'MET1', 'f': 'LYS3', 'i': 'MET3', 'h': 'MET2', 'k': 'PHE2', 'j': 'PHE1', 'm': 'PRO1', 'l': 'PHE3', 'o': 'SER1', 'n': 'PRO2', 'q': 'SER3', 'p': 'SER2', 's': 'THR2', 'r': 'THR1', 'u': 'TRP1', 't': 'THR3', 'w': 'TRP3', 'v': 'TRP2', 'y': 'TYR2', 'x': 'TYR1', 'z': 'TYR3'}

four_rota_to_one_dict = dict([[v,k] for k,v in one_rota_to_four_dict.items()])

three_to_one = {'CYS': 'C', 'ASP': 'D', 'SER': 'S', 'GLN': 'Q', 'LYS': 'K', 'TRP': 'W', 'PRO': 'P', 'THR': 'T', 'ILE': 'I', 'ALA': 'A', 'PHE': 'F', 'GLY': 'G', 'HIS': 'H', 'LEU': 'L', 'ARG': 'R', 'MET': 'M', 'GLU': 'E', 'ASN': 'N', 'TYR': 'Y', 'VAL': 'V'}

one_to_three_dict = dict([[v,k] for k,v in three_to_one.items()])

masking_dict = {'1': 'V', '0': 'V', '2': 'V', 'A': 'A', 'C': 'R', 'B': 'R', 'E': 'N', 'D': 'R', 'G': 'N', 'F': 'N', 'I': 'D', 'H': 'D', 'K': 'C', 'J': 'D', 'M': 'C', 'L': 'C', 'O': 'Q', 'N': 'Q', 'Q': 'E', 'P': 'Q', 'S': 'E', 'R': 'E', 'U': 'H', 'T': 'G', 'W': 'H', 'V': 'H', 'Y': 'I', 'X': 'I', 'Z': 'I', 'a': 'L', 'c': 'L', 'b': 'L', 'e': 'K', 'd': 'K', 'g': 'M', 'f': 'K', 'i': 'M', 'h': 'M', 'k': 'F', 'j': 'F', 'm': 'P', 'l': 'F', 'o': 'S', 'n': 'P', 'q': 'S', 'p': 'S', 's': 'T', 'r': 'T', 'u': 'W', 't': 'T', 'w': 'W', 'v': 'W', 'y': 'Y', 'x': 'Y', 'z': 'Y'}


def main():
	parser = argparse.ArgumentParser()

	parser.add_argument('-m', '--mask', default=False, action='store_true', help='mask a rotasequence (1-character symbols) to make it look like a AA sequnce (1-character symbols) [default: %(default)s]')
	parser.add_argument('-M', '--Mask_custom', type=str, default=False, help='mask using a custom dictionary provided in a yaml file')
	parser.add_argument('-t', '--three', dest='three', action='store_true', default=False, help='translate codons into  three-letter AA symbols [default: %(default)s]')	
	parser.add_argument('-r', '--rotamer', dest='rotamer', action='store_true', default=False, help='translate a sequence of codons into rotamer states (55x55) [default: %(default)s]')	
	parser.add_argument('-i', '--invert', dest='invert', action='store_true', default=False, help='translate a sequence of AA (1-letter) into codons [default: %(default)s]')	
	parser.add_argument('-o', '--one_to_three', dest='one_to_three', action='store_true', default=False, help='1-letter AA to 3-letter AA [default: %(default)s]')
	parser.add_argument('-P', '--PAML', dest='PAML', action='store_true', default=False, help='3-letter AA to 1-letter AA [default: %(default)s]')
	parser.add_argument('-c', '--convert', action='store_true', default=False, help='convert rotamer (1-character) states into codons')
	parser.add_argument('-C', '--CONVERT', action='store_true', default=False, help='convert rotamers (4-character states) into 1-character')
	parser.add_argument('-s', '--shorten', action='store_true', default=False, help='translate a sequence of 3-letter AA into 1-letter AA (space-separated)  [default: %(default)s]')
	parser.add_argument('-l', '--list_taxa',  default=False, type=str, help='specify a list of taxa to convert, the others are left unchanged')
	args = parser.parse_args()
	
	if args.Mask_custom:
		f = open(args.Mask_custom, "r")
		custom_masking_dict = yaml.load(f)
		f.close()

	taxa_to_convert = []
	if args.list_taxa:
		taxa_to_convert = args.list_taxa.split(",")

	for line in stdin:
		out = []
		line = line.replace("\r", "")
		if line != '\n' and line != "\t":
			ID, field2 = line.rstrip("\n").split("\t")	
			if field2.isdigit():
				if args.invert or args.convert:
					nseq, lenseq = line.rstrip("\n").split("\t")
					print("{}\t{}".format(nseq, int(lenseq)*3))
				elif args.rotamer or args.three:
					nseq, lenseq = line.rstrip("\n").split("\t")
					print("{}\t{}".format(nseq, int(lenseq)/3))
				else:
					print(line)
			else:
				if args.list_taxa and ID not in taxa_to_convert:
					print(line)
					continue
				else:
					# symbols  must be space separated or "," separated unless they are 1-letter AAs
					s1 = field2.split(' ')
					s2 = field2.split(',')
					if len(s1) > len(s2): 
						sequence = s1
					elif len(s2) > len(s1):
						sequence = s2
					else:
						if args.invert or args.one_to_three:
							sequence = list(field2)
						else:
							print(line)
							continue
					for symbol in sequence:
						if symbol == '-':
							out.append('-')
						else:
							if args.three:
								out.append(codon_AA_dict[symbol])
							elif args.PAML:
								out.append(three_to_one[codon_AA_dict[symbol]])
							elif args.Mask_custom:
								out.append(custom_masking_dict[symbol])
							elif args.mask:
								out.append(masking_dict[symbol])
							elif args.rotamer:
								out.append(codon_rotamer_dict[symbol])
							elif args.invert:
								# from 1-letter AA to space-separated codons
								out.append(AA_codon_dict[one_to_three_dict[symbol]])
							elif args.convert:
								out.append(rotamer_codon_dict[one_rota_to_four_dict[symbol]])
							elif args.CONVERT:
								out.append(four_rota_to_one_dict[symbol])
							elif args.one_to_three:
								out.append(one_to_three_dict[symbol])
							elif args.shorten:
								out.append(three_to_one[symbol])
							else:	
								out.append(three_to_one[codon_AA_dict[symbol]])
					if args.PAML:
						# no spaces in sequence
						line = ID + '\t'  + ''.join(map(str, out))
					else:
						line = ID + '\t'  + ' '.join(map(str, out))
					print(line)
		else:
			print(line)

if __name__ == '__main__':
	main()


